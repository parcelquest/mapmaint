Option Strict Off
Option Explicit On
Module CountyInfo
	
	Structure TCountyInfo
		Dim CountyCode As String
		Dim CountyID As String
      Dim ApnLen As Integer
      Dim BookLen As Integer
      Dim PageLen As Integer
      Dim CmpLen As Integer
      Dim MaxBook As String
		Dim CountyName As String
		Dim ApnFormat As String
	End Structure
   Public Const MAX_COUNTIES As Integer = 64
	
	Public asCountyInfo(MAX_COUNTIES) As TCountyInfo
   Public iNumCounties As Integer
   Private m_iCurCnty As Integer
	
	Public Function LoadCountyInfo(ByVal CtyFile As String) As Boolean
		Dim bRet As Boolean
      Dim fh, iRet As Integer
		Dim sTmp As String
		Dim asFlds() As String
		
		On Error GoTo LoadCountyInfo_Error
		
		fh = FreeFile
		
		FileOpen(fh, CtyFile, OpenMode.Input)
		sTmp = LineInput(fh)
		iNumCounties = 0
		
		Do While Not EOF(fh)
			sTmp = LineInput(fh)
			'CountyCode , CountyID, ApnLen, BookLen, PageLen, CmpLen, CountyName, ApnFormat
         asFlds = sTmp.Split(",")
         If asFlds.Length > 1 Then
            iNumCounties = iNumCounties + 1
            asCountyInfo(iNumCounties).CountyCode = asFlds(0)
            asCountyInfo(iNumCounties).CountyID = asFlds(1)
            asCountyInfo(iNumCounties).ApnLen = CInt(asFlds(2))
            asCountyInfo(iNumCounties).BookLen = CInt(asFlds(3))
            asCountyInfo(iNumCounties).PageLen = CInt(asFlds(4))
            asCountyInfo(iNumCounties).CmpLen = CInt(asFlds(5))
            asCountyInfo(iNumCounties).CountyName = asFlds(6)
            asCountyInfo(iNumCounties).ApnFormat = asFlds(7)
            asCountyInfo(iNumCounties).MaxBook = asFlds(8)
         Else
            Exit Do
         End If
		Loop 
		FileClose(fh)
		
		LoadCountyInfo = True
		Exit Function
LoadCountyInfo_Error: 
		If fh > 0 Then FileClose(fh)
		LoadCountyInfo = False
	End Function
	
   Public Function getBookLen(ByVal sCntyCode As String) As Integer
      Dim iTmp, iRet As Integer

      iRet = 0
      If iNumCounties > 0 Then
         For iTmp = 1 To iNumCounties
            If asCountyInfo(iTmp).CountyCode = sCntyCode Then
               iRet = asCountyInfo(iTmp).BookLen
               Exit For
            End If
         Next
      End If
      getBookLen = iRet
   End Function
	
   Public Function getPageLen(ByVal sCntyCode As String) As Integer
      Dim iTmp, iRet As Integer

      iRet = 0
      If iNumCounties > 0 Then
         For iTmp = 1 To iNumCounties
            If asCountyInfo(iTmp).CountyCode = sCntyCode Then
               iRet = asCountyInfo(iTmp).PageLen
               Exit For
            End If
         Next
      End If
      getPageLen = iRet
   End Function
	
   Public Function getBookPageLen(ByVal sCntyCode As String) As Integer
      Dim iTmp, iRet As Integer

      iRet = 0
      If iNumCounties > 0 Then
         For iTmp = 1 To iNumCounties
            If asCountyInfo(iTmp).CountyCode = sCntyCode Then
               iRet = asCountyInfo(iTmp).BookLen + asCountyInfo(iTmp).PageLen
               Exit For
            End If
         Next
      End If
      getBookPageLen = iRet
   End Function
	
   Public Function getCountyCode(ByVal iCntyID As Integer) As String
      Dim sRet As String

      sRet = ""
      If iCntyID <= iNumCounties And iCntyID > 0 Then
         sRet = asCountyInfo(iCntyID).CountyCode
      End If
      getCountyCode = sRet
   End Function

   Public Function getCountyMaxBook(Optional ByVal iCntyID As Integer = 0) As String
      Dim sRet As String

      sRet = ""
      If iCntyID = 0 And m_iCurCnty > 0 And m_iCurCnty <= iNumCounties Then
         sRet = asCountyInfo(m_iCurCnty).MaxBook
      ElseIf iCntyID <= iNumCounties And iCntyID > 0 Then
         sRet = asCountyInfo(iCntyID).MaxBook
      End If
      getCountyMaxBook = sRet
   End Function

   Public Function setCurrentCounty(ByVal sCntyCode As String) As Integer
      Dim iTmp As Integer

      m_iCurCnty = 0
      If iNumCounties > 0 Then
         For iTmp = 1 To iNumCounties
            If asCountyInfo(iTmp).CountyCode = sCntyCode Then
               m_iCurCnty = iTmp
               Exit For
            End If
         Next
      End If
      setCurrentCounty = m_iCurCnty
   End Function

End Module