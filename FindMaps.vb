Option Strict Off
Option Explicit On
Module FindMaps
	
	'Search for index maps.  Log missing ones
	Public Function FindMissingIndex(ByVal sCntyCode As String) As Boolean
      Dim fhOut, fhIn, iRet As Integer
      Dim iBookLen, iPageLen As Integer
		Dim sTmp, sMapDir As String
		Dim sOutfile, sCmpfile, sFilename As String
		Dim asFlds() As String
      Dim sBook, sPage, sMaxBook As String
		
		On Error GoTo FindMissingIndex_Error
		
		'Get book/page len
		iBookLen = getBookLen(sCntyCode)
		iPageLen = getPageLen(sCntyCode)
		If iBookLen = 0 Or iPageLen = 0 Then
			MsgBox("Invalid Book/Page length.  Please check " & g_sCountyFile)
			Exit Function
		End If
		
		'Setup outfile
      sOutfile = g_sOutputDir.Replace("???", sCntyCode)
      If Dir(sOutfile, FileAttribute.Directory) = "" Then
         MkDir((sOutfile))
      End If
		sOutfile = sOutfile & "\" & sCntyCode & "_MissingIndex.log"
		fhOut = FreeFile
		FileOpen(fhOut, sOutfile, OpenMode.Output)
		
		'Open CMP file
		sCmpfile = g_sDataDir & "\S" & sCntyCode & "assr\S" & sCntyCode & ".cmp"
		fhIn = FreeFile
		FileOpen(fhIn, sCmpfile, OpenMode.Input)
		
		'Setup map folder
		sMapDir = g_sMapDir & "\" & sCntyCode & "\BK"
		sPage = Left("0000", iPageLen)
		
      'Loop through all record in CMP
      sBook = " "
      sMaxBook = getCountyMaxBook()
      g_iMissingIndex = 0
		Do While Not EOF(fhIn)
			sTmp = LineInput(fhIn)
         If sBook <> Left(sTmp, iBookLen) Then
            sBook = Left(sTmp, iBookLen)
            sFilename = sMapDir & sBook & "\" & sBook & sPage & "." & g_sMapExt
            If sBook > "0" And sBook < sMaxBook Then
               If Dir(sFilename, FileAttribute.Normal) = "" Then
                  PrintLine(fhOut, sBook)
                  g_iMissingIndex = g_iMissingIndex + 1
                  LogMsg0(sFilename)
               End If
            End If
         End If
      Loop
		
		FileClose(fhIn)
      FileClose(fhOut)

      FindMissingIndex = True
		Exit Function
		
FindMissingIndex_Error: 
		LogMsg("Error: " & Err.Description)
		FindMissingIndex = False
   End Function

   Public Function FindMissingMaps(ByVal sCntyCode As String, ByVal bPath As Boolean) As Boolean
      Dim fhOut, fhIn, iRet As Integer
      Dim iBookLen, iCmpLen, iPageLen As Integer
      Dim sTmp, sMapDir As String
      Dim sOutfile, sCmpfile, sFilename As String
      Dim asFlds() As String
      Dim sBook, sBookPage, sPage, sMaxBook As String
      Dim sPathInc As String

      On Error GoTo FindMissingMaps_Error

      'Get book/page len
      iBookLen = getBookLen(sCntyCode)
      iPageLen = getPageLen(sCntyCode)
      If iBookLen = 0 Or iPageLen = 0 Then
         MsgBox("Invalid Book/Page length.  Please check " & g_sCountyFile)
         Exit Function
      End If
      iCmpLen = iBookLen + iPageLen

      'Setup outfile
      sOutfile = g_sOutputDir.Replace("???", sCntyCode)
      If Dir(sOutfile, FileAttribute.Directory) = "" Then
         MkDir((sOutfile))
      End If
      sOutfile = sOutfile & "\" & sCntyCode & "_MissingMaps.log"
      fhOut = FreeFile()
      FileOpen(fhOut, sOutfile, OpenMode.Output)

      'Open CMP file
      sCmpfile = g_sDataDir & "\S" & sCntyCode & "assr\S" & sCntyCode & ".cmp"
      fhIn = FreeFile()
      FileOpen(fhIn, sCmpfile, OpenMode.Input)

      'Setup map folder
      sBookPage = " "
      sMapDir = g_sMapDir & "\" & sCntyCode & "\BK"
      g_iMissingMap = 0
      sMaxBook = getCountyMaxBook()

      'Loop through all record in CMP
      Do While Not EOF(fhIn)
         sTmp = LineInput(fhIn)
         sBook = Left(sTmp, iBookLen)
         If sBookPage <> Left(sTmp, iCmpLen) And sBook < sMaxBook Then
            sBookPage = Left(sTmp, iCmpLen)
            sPage = Mid(sTmp, iBookLen + 1, iPageLen)
            sFilename = sMapDir & sBook & "\" & sBook & sPage & "." & g_sMapExt
            If Dir(sFilename, FileAttribute.Normal) = "" Then
               If bPath Then
                  PrintLine(fhOut, "BK" & sBook & "\" & sBookPage & ".PDF")
               Else
                  PrintLine(fhOut, sBookPage)
               End If
               g_iMissingMap = g_iMissingMap + 1
               LogMsg0(sFilename)
            End If
         End If
      Loop

      FileClose(fhIn)
      FileClose(fhOut)
      FindMissingMaps = True
      Exit Function

FindMissingMaps_Error:
      LogMsg("Error: " & Err.Description)
      FindMissingMaps = False
   End Function
	
	'This is exception #1 where alpha character may or may not in the book field
	Public Function Ex1MissingIndex(ByVal sCntyCode As String) As Boolean
		Dim fhOut, fhIn, iRet As Short
		Dim iBookLen, iPageLen As Short
		Dim sTmp, sMapDir As String
		Dim sOutfile, sCmpfile, sFilename As String
		Dim asFlds() As String
		Dim sBook, sPage As String
		
		On Error GoTo Ex1MissingIndex_Error
		
		'Get book/page len
		iBookLen = getBookLen(sCntyCode)
		iPageLen = getPageLen(sCntyCode)
		If iBookLen = 0 Or iPageLen = 0 Then
			MsgBox("Invalid Book/Page length.  Please check " & g_sCountyFile)
			Exit Function
		End If
		
		'Setup outfile
      sOutfile = g_sOutputDir.Replace("???", sCntyCode)
      If Dir(sOutfile, FileAttribute.Directory) = "" Then
         MkDir((sOutfile))
      End If
		sOutfile = sOutfile & "\" & sCntyCode & "_MissingIndex.log"
		fhOut = FreeFile
		FileOpen(fhOut, sOutfile, OpenMode.Output)
		
		'Open CMP file
		sCmpfile = g_sDataDir & "\S" & sCntyCode & "assr\S" & sCntyCode & ".cmp"
		fhIn = FreeFile
		FileOpen(fhIn, sCmpfile, OpenMode.Input)
		
		'Setup map folder
		sMapDir = g_sMapDir & "\" & sCntyCode & "\BK"
		sPage = Left("0000", iPageLen)
      g_iMissingIndex = 0

      'Loop through all record in CMP
      sBook = " "
		Do While Not EOF(fhIn)
			sTmp = LineInput(fhIn)
			iRet = IIf(Mid(sTmp, iBookLen, 1) > "9", iBookLen, iBookLen - 1)
			If sBook <> Left(sTmp, iRet) Then
				sBook = Left(sTmp, iRet)
				sFilename = sMapDir & sBook & "\" & sBook & sPage & "." & g_sMapExt
            If Dir(sFilename, FileAttribute.Normal) = "" Then
               PrintLine(fhOut, sBook)
               g_iMissingIndex = g_iMissingIndex + 1
               LogMsg(sFilename)
            End If
			End If
		Loop 
		
		FileClose(fhIn)
		FileClose(fhOut)
		Ex1MissingIndex = True
		Exit Function
		
Ex1MissingIndex_Error: 
		LogMsg("Error: " & Err.Description)
		Ex1MissingIndex = False
	End Function
	
	Public Function Ex1MissingMaps(ByVal sCntyCode As String) As Boolean
      Dim iRet, fhIn, fhOut, iTmp, iCnt As Integer
      Dim iBookLen, iCmpLen, iPageLen As Integer
		Dim sTmp, sMapDir As String
		Dim sOutfile, sCmpfile, sFilename As String
		Dim asFlds() As String
		Dim sBook, sBookPage, sPage As String
		
		On Error GoTo Ex1MissingMaps_Error
		
		'Get book/page len
		iBookLen = getBookLen(sCntyCode)
		iPageLen = getPageLen(sCntyCode)
		If iBookLen = 0 Or iPageLen = 0 Then
			MsgBox("Invalid Book/Page length.  Please check " & g_sCountyFile)
			Exit Function
		End If
		iCmpLen = iBookLen + iPageLen
		
		'Setup outfile
      sOutfile = g_sOutputDir.Replace("???", sCntyCode)
      If Dir(sOutfile, FileAttribute.Directory) = "" Then
         MkDir((sOutfile))
      End If
		sOutfile = sOutfile & "\" & sCntyCode & "_MissingMaps.log"
		fhOut = FreeFile
		FileOpen(fhOut, sOutfile, OpenMode.Output)
		
		'Open CMP file
		sCmpfile = g_sDataDir & "\S" & sCntyCode & "assr\S" & sCntyCode & ".cmp"
		fhIn = FreeFile
		FileOpen(fhIn, sCmpfile, OpenMode.Input)
		
		'Setup map folder
		sBookPage = " "
		sMapDir = g_sMapDir & "\" & sCntyCode & "\BK"
      g_iMissingMap = 0
      iCnt = 0

		'Loop through all record in CMP
		Do While Not EOF(fhIn)
         sTmp = LineInput(fhIn)
         iCnt += 1

			'If there is alpha, we keep book length the same.  Otherwise, subtract 1
			If Mid(sTmp, iBookLen, 1) > "9" Then
				iTmp = 0
			Else
				iTmp = 1
			End If
			
			'iCmpLen and iBookLen is adjustable based on last character on Book field
			'iRet = iCmpLen - iTmp
			If sBookPage <> Left(sTmp, iCmpLen) Then
				sBookPage = Left(sTmp, iCmpLen)
				'iRet = iBookLen - iTmp
				sBook = Left(sTmp, iBookLen - iTmp)
				sPage = Mid(sTmp, iBookLen + 1, iPageLen)
				sFilename = sMapDir & sBook & "\" & sBook & sPage & "." & g_sMapExt
            If Dir(sFilename, FileAttribute.Normal) = "" Then
               PrintLine(fhOut, sBookPage)
               g_iMissingMap = g_iMissingMap + 1
               LogMsg(sFilename)
            End If
			End If
		Loop 
		
		FileClose(fhIn)
      FileClose(fhOut)
      LogMsg("Number of records processed: " & iCnt)
      LogMsg("Number of missing maps:      " & g_iMissingMap)

		Ex1MissingMaps = True
		Exit Function
		
Ex1MissingMaps_Error: 
		LogMsg("Error: " & Err.Description)
		Ex1MissingMaps = False
	End Function
	
	Public Function LAK_MissingMaps(ByVal sCntyCode As String) As Boolean
      Dim fhOut, fhIn, iRet As Integer
      Dim iBookLen, iCmpLen, iPageLen, iSaveCmpLen, iCnt As Integer
      Dim iBook, iPage As Integer
		Dim sTmp, sMapDir As String
		Dim sOutfile, sCmpfile, sFilename As String
		Dim asFlds() As String
      Dim sBook, sBookPage, sPage, sMaxBook As String
		
		On Error GoTo LAK_MissingMaps_Error
		
		'Get book/page len
		iBookLen = getBookLen(sCntyCode)
		iPageLen = getPageLen(sCntyCode)
		If iBookLen = 0 Or iPageLen = 0 Then
			MsgBox("Invalid Book/Page length.  Please check " & g_sCountyFile)
			Exit Function
		End If
		iCmpLen = iBookLen + iPageLen
		
		'Setup outfile
      sOutfile = g_sOutputDir.Replace("???", sCntyCode)
      If Dir(sOutfile, FileAttribute.Directory) = "" Then
         MkDir((sOutfile))
      End If
		sOutfile = sOutfile & "\" & sCntyCode & "_MissingMaps.log"
		fhOut = FreeFile
		FileOpen(fhOut, sOutfile, OpenMode.Output)
		
		'Open CMP file
		sCmpfile = g_sDataDir & "\S" & sCntyCode & "assr\S" & sCntyCode & ".cmp"
		fhIn = FreeFile
		FileOpen(fhIn, sCmpfile, OpenMode.Input)
		
		'Setup map folder
      sBookPage = " "
      sMaxBook = getCountyMaxBook()
		sMapDir = g_sMapDir & "\" & sCntyCode & "\BK"
      iSaveCmpLen = iCmpLen

		'Loop through all record in CMP
		Do While Not EOF(fhIn)
         sTmp = LineInput(fhIn)
         iCnt += 1
         sBook = Left(sTmp, iBookLen)
         sPage = Mid(sTmp, iBookLen + 1, iPageLen + 1)
         iBook = CInt(sBook)
         iPage = CInt(sPage)
         If (iBook = 6 And iPage > 33) Or
            (iBook = 8 And iPage > 65) Or
            (iBook = 14 And iPage > 6) Or
            (iBook > 23 And iBook < 115) Or
            (iBook > 121 And iBook < 629) Or
            (iBook > 803 And iBook < 805) Then
            iCmpLen = iSaveCmpLen
            sPage = Mid(sTmp, iBookLen + 1, iPageLen)
         Else
            iCmpLen = iSaveCmpLen + 1
            sPage = Mid(sTmp, iBookLen + 2, iPageLen)
         End If

         If sBookPage <> Left(sTmp, iCmpLen) And sBook < sMaxBook Then
            sBookPage = Left(sTmp, iCmpLen)

            sFilename = sMapDir & sBook & "\" & sBook & sPage & "." & g_sMapExt
            If Dir(sFilename, FileAttribute.Normal) = "" Then
               PrintLine(fhOut, sBook & sPage)
               LogMsg(sFilename)
               g_iMissingMap += 1
            End If
         End If
      Loop
		
		FileClose(fhIn)
      FileClose(fhOut)
      LogMsg("Number of records processed: " & iCnt)
      LogMsg("Number of missing maps:      " & g_iMissingMap)

		LAK_MissingMaps = True
		Exit Function
		
LAK_MissingMaps_Error: 
      LogMsg("***** Error: " & Err.Description)
		LAK_MissingMaps = False
	End Function

   'This function is not working yet
   Public Function LAK_MissingIndex(ByVal sCntyCode As String) As Boolean
      Dim fhOut, fhIn, iRet As Integer
      Dim iBookLen, iPageLen, iBook, iPage As Integer
      Dim sTmp, sMapDir As String
      Dim sOutfile, sCmpfile, sFilename As String
      Dim asFlds() As String
      Dim sBook, sPage, sMaxBook As String

      On Error GoTo LAK_MissingIndex_Error

      'Get book/page len
      iBookLen = getBookLen(sCntyCode)
      iPageLen = getPageLen(sCntyCode)
      If iBookLen = 0 Or iPageLen = 0 Then
         MsgBox("Invalid Book/Page length.  Please check " & g_sCountyFile)
         Exit Function
      End If

      'Setup outfile
      sOutfile = g_sOutputDir.Replace("???", sCntyCode)
      If Dir(sOutfile, FileAttribute.Directory) = "" Then
         MkDir((sOutfile))
      End If
      sOutfile = sOutfile & "\" & sCntyCode & "_MissingIndex.log"
      fhOut = FreeFile()
      FileOpen(fhOut, sOutfile, OpenMode.Output)

      'Open CMP file
      sCmpfile = g_sDataDir & "\S" & sCntyCode & "assr\S" & sCntyCode & ".cmp"
      fhIn = FreeFile()
      FileOpen(fhIn, sCmpfile, OpenMode.Input)

      'Setup map folder
      sMapDir = g_sMapDir & "\" & sCntyCode & "\BK"

      'Loop through all record in CMP
      sBook = " "
      sMaxBook = getCountyMaxBook()
      g_iMissingIndex = 0

      Do While Not EOF(fhIn)
         sTmp = LineInput(fhIn)
         If sBook <> Left(sTmp, iBookLen) Then
            sBook = Left(sTmp, iBookLen)
            iBook = CInt(sBook)
            iPage = CInt(Mid(sTmp, iBookLen + 1, iPageLen + 1))
            If (iBook = 6 And iPage > 33) Or
               (iBook = 8 And iPage > 65) Or
               (iBook = 14 And iPage > 6) Or
               (iBook > 23 And iBook < 115) Or
               (iBook > 121 And iBook < 629) Or
               (iBook > 803 And iBook < 805) Then
               sPage = Left("0000", iPageLen)
            Else
               sPage = Left("0000", iPageLen + 1)
            End If

            sFilename = sMapDir & sBook & "\" & sBook & sPage & "." & g_sMapExt
            If sBook > "0" And sBook < sMaxBook Then
               If Dir(sFilename, FileAttribute.Normal) = "" Then
                  PrintLine(fhOut, sBook)
                  g_iMissingIndex = g_iMissingIndex + 1
                  LogMsg0(sFilename)
               End If
            End If
         End If
      Loop

      FileClose(fhIn)
      FileClose(fhOut)
      LogMsg("Number of missing index:      " & g_iMissingIndex)

      LAK_MissingIndex = True
      Exit Function

LAK_MissingIndex_Error:
      LogMsg("Error: " & Err.Description)
      LAK_MissingIndex = False
   End Function

	Public Function ListAllMaps(ByVal sCntyCode As String, ByVal bPath As Boolean) As Boolean
		Dim fhOut, fhIn, iRet As Short
		Dim iBookLen, iCmpLen, iPageLen As Short
		Dim sTmp, sMapDir As String
		Dim sOutfile, sCmpfile, sFilename As String
		Dim asFlds() As String
		Dim sBook, sBookPage, sPage As String
		Dim sPathInc As String
		Dim sWebPath As String
		Dim sMapIndex As String
		
		On Error GoTo ListAllMaps_Error
		
		'Set default
		sWebPath = "http://www.co.stanislaus.ca.us/ASSESSOR/MapBooks/"
		
		'Get book/page len
		iBookLen = getBookLen(sCntyCode)
		iPageLen = getPageLen(sCntyCode)
		If iBookLen = 0 Or iPageLen = 0 Then
			MsgBox("Invalid Book/Page length.  Please check " & g_sCountyFile)
			Exit Function
		End If
		iCmpLen = iBookLen + iPageLen
		
		'Setup outfile
      sOutfile = g_sOutputDir.Replace("???", sCntyCode)
      If Dir(sOutfile, FileAttribute.Directory) = "" Then
         MkDir((sOutfile))
      End If
		sOutfile = sOutfile & "\" & sCntyCode & "_AllMaps.log"
		fhOut = FreeFile
		FileOpen(fhOut, sOutfile, OpenMode.Output)
		
		'Open CMP file
		sCmpfile = g_sDataDir & "\S" & sCntyCode & "assr\S" & sCntyCode & ".cmp"
		fhIn = FreeFile
		FileOpen(fhIn, sCmpfile, OpenMode.Input)
		
		'County index
		If bPath Then
			PrintLine(fhOut, sWebPath & New String("0", iCmpLen) & "." & g_sMapExt)
		End If
		
		'Setup map folder
		sBook = " "
		sBookPage = " "
		sMapIndex = New String("0", iPageLen)
		sWebPath = sWebPath & "BK"
		
		'Loop through all record in CMP
		Do While Not EOF(fhIn)
			sTmp = LineInput(fhIn)
			
			'Check for index maps
			If sBook <> Left(sTmp, iBookLen) Then
				sBook = Left(sTmp, iBookLen)
				If bPath Then
					PrintLine(fhOut, sWebPath & sBook & "/" & sBook & sMapIndex & "." & g_sMapExt)
				Else
					PrintLine(fhOut, sBook)
				End If
			End If
			
			'Check for map page
			If sBookPage <> Left(sTmp, iCmpLen) Then
				sBookPage = Left(sTmp, iCmpLen)
				sBook = Left(sTmp, iBookLen)
				If bPath Then
					PrintLine(fhOut, sWebPath & sBook & "/" & sBookPage & "." & g_sMapExt)
				Else
					PrintLine(fhOut, sBookPage)
				End If
			End If
		Loop 
		
		FileClose(fhIn)
		FileClose(fhOut)
		ListAllMaps = True
		Exit Function
		
ListAllMaps_Error: 
		LogMsg("Error: " & Err.Description)
		ListAllMaps = False
   End Function

   'SBX
   Public Function SBX_MissingMaps(ByVal sCntyCode As String) As Boolean
      Dim fhOut, fhIn, iRet, iCnt As Integer
      Dim iBookLen, iCmpLen, iPageLen, iBook As Integer
      Dim sTmp, sMapDir As String
      Dim sOutfile, sCmpfile, sFilename As String
      Dim asFlds() As String
      Dim sBook, sBookPage, sPage, sMaxBook As String
      Dim sPathInc As String

      On Error GoTo FindMissingMaps_Error

      'Get book/page len
      iBookLen = getBookLen(sCntyCode)
      iPageLen = getPageLen(sCntyCode)
      If iBookLen = 0 Or iPageLen = 0 Then
         MsgBox("Invalid Book/Page length.  Please check " & g_sCountyFile)
         Exit Function
      End If
      iCmpLen = iBookLen + iPageLen

      'Setup outfile
      sOutfile = g_sOutputDir.Replace("???", sCntyCode)
      If Dir(sOutfile, FileAttribute.Directory) = "" Then
         MkDir((sOutfile))
      End If
      sOutfile = sOutfile & "\" & sCntyCode & "_MissingMaps.log"
      fhOut = FreeFile()
      FileOpen(fhOut, sOutfile, OpenMode.Output)

      'Open CMP file
      sCmpfile = g_sDataDir & "\S" & sCntyCode & "assr\S" & sCntyCode & ".cmp"
      fhIn = FreeFile()
      FileOpen(fhIn, sCmpfile, OpenMode.Input)

      'Setup map folder
      sBookPage = " "
      sMapDir = g_sMapDir & "\" & sCntyCode & "\BK"
      g_iMissingMap = 0
      iCnt = 0
      sMaxBook = getCountyMaxBook()

      'Loop through all record in CMP
      Do While Not EOF(fhIn)
         sTmp = LineInput(fhIn)
         iCnt += 1
         sBook = Left(sTmp, iBookLen)
         iBook = CInt(sBook)
         If sBookPage <> Left(sTmp, Len(sBookPage)) And sBook < sMaxBook Then
            If iBook = 4 Or iBook = 98 Or iBook = 118 Or iBook = 128 Or (iBook > 499 And iBook < 700) Then
               sPage = Mid(sTmp, iBookLen + 1, iPageLen + 1)
               sBookPage = Left(sTmp, iCmpLen + 1)
            Else
               sPage = Mid(sTmp, iBookLen + 1, iPageLen)
               sBookPage = Left(sTmp, iCmpLen)
            End If
            sFilename = sMapDir & sBook & "\" & sBook & sPage & "." & g_sMapExt
            If Dir(sFilename, FileAttribute.Normal) = "" Then
               PrintLine(fhOut, sBookPage)
               g_iMissingMap = g_iMissingMap + 1
               LogMsg0(sFilename)
            End If
         End If
      Loop

      LogMsg("Number of records processed: " & iCnt)
      LogMsg("Number of missing maps:      " & g_iMissingMap)
      FileClose(fhIn)
      FileClose(fhOut)
      SBX_MissingMaps = True
      Exit Function

FindMissingMaps_Error:
      LogMsg("Error: " & Err.Description)
      SBX_MissingMaps = False
   End Function

   'SFX
   Public Function SFX_MissingMaps(ByVal sCntyCode As String) As Boolean
      Dim fhOut, fhIn, iRet, iCnt As Integer
      Dim iBookLen, iCmpLen, iPageLen, iBook As Integer
      Dim sTmp, sOneChar, sMapDir As String
      Dim sOutfile, sCmpfile, sFilename As String
      Dim asFlds() As String
      Dim sBook, sBookPage, sPage, sMaxBook As String
      Dim sPathInc As String

      On Error GoTo SFX_MissingMaps_Error

      'Get book/page len
      iBookLen = getBookLen(sCntyCode)
      iPageLen = getPageLen(sCntyCode)
      If iBookLen = 0 Or iPageLen = 0 Then
         MsgBox("Invalid Book/Page length.  Please check " & g_sCountyFile)
         Exit Function
      End If
      iCmpLen = iBookLen + iPageLen

      'Setup outfile
      sOutfile = g_sOutputDir.Replace("???", sCntyCode)
      If Dir(sOutfile, FileAttribute.Directory) = "" Then
         MkDir((sOutfile))
      End If
      sOutfile = sOutfile & "\" & sCntyCode & "_MissingMaps.log"
      fhOut = FreeFile()
      FileOpen(fhOut, sOutfile, OpenMode.Output)

      'Open CMP file
      sCmpfile = g_sDataDir & "\S" & sCntyCode & "assr\S" & sCntyCode & ".cmp"
      fhIn = FreeFile()
      FileOpen(fhIn, sCmpfile, OpenMode.Input)

      'Setup map folder
      sBookPage = " "
      sMapDir = g_sMapDir & "\" & sCntyCode & "\BK"
      g_iMissingMap = 0
      iCnt = 0
      sMaxBook = getCountyMaxBook()

      'Loop through all record in CMP
      Do While Not EOF(fhIn)
         sTmp = LineInput(fhIn)
         sBook = Left(sTmp, iBookLen)
         If sBookPage <> Left(sTmp, iCmpLen) And sBook < sMaxBook Then
            sBookPage = Left(sTmp, iCmpLen)
            sOneChar = Mid(sTmp, iBookLen + 3, 1)
            If sOneChar < "A" Then
               sPage = Mid(sTmp, iBookLen + 1, iPageLen - 1)
            Else
               sPage = Mid(sTmp, iBookLen + 1, iPageLen)
            End If

            sFilename = sMapDir & sBook & "\" & sBook & sPage & "." & g_sMapExt
            If Dir(sFilename, FileAttribute.Normal) = "" Then
               PrintLine(fhOut, sBookPage)
               g_iMissingMap = g_iMissingMap + 1
               LogMsg0(sFilename)
            End If
         End If
      Loop

      LogMsg("Number of records processed: " & iCnt)
      LogMsg("Number of missing maps:      " & g_iMissingMap)
      FileClose(fhIn)
      FileClose(fhOut)
      SFX_MissingMaps = True
      Exit Function

SFX_MissingMaps_Error:
      LogMsg("Error: " & Err.Description)
      SFX_MissingMaps = False
   End Function

   'Search for index maps.  Log missing ones
   Public Function SFX_MissingIndex(ByVal sCntyCode As String) As Boolean
      Dim fhOut, fhIn, iRet As Integer
      Dim iBookLen, iPageLen As Integer
      Dim sTmp, sMapDir As String
      Dim sOutfile, sCmpfile, sFilename As String
      Dim asFlds() As String
      Dim sBook, sPage, sMaxBook As String

      On Error GoTo Sfx_MissingIndex_Error

      'Get book/page len
      iBookLen = getBookLen(sCntyCode)
      iPageLen = getPageLen(sCntyCode)
      If iBookLen = 0 Or iPageLen = 0 Then
         MsgBox("Invalid Book/Page length.  Please check " & g_sCountyFile)
         Exit Function
      End If

      'Setup outfile
      sOutfile = g_sOutputDir.Replace("???", sCntyCode)
      If Dir(sOutfile, FileAttribute.Directory) = "" Then
         MkDir((sOutfile))
      End If
      sOutfile = sOutfile & "\" & sCntyCode & "_MissingIndex.log"
      fhOut = FreeFile()
      FileOpen(fhOut, sOutfile, OpenMode.Output)

      'Open CMP file
      sCmpfile = g_sDataDir & "\S" & sCntyCode & "assr\S" & sCntyCode & ".cmp"
      fhIn = FreeFile()
      FileOpen(fhIn, sCmpfile, OpenMode.Input)

      'Setup map folder
      sMapDir = g_sMapDir & "\" & sCntyCode & "\BK"
      sPage = Left("0000", iPageLen - 1)

      'Loop through all record in CMP
      sBook = " "
      sMaxBook = getCountyMaxBook()
      g_iMissingIndex = 0
      Do While Not EOF(fhIn)
         sTmp = LineInput(fhIn)
         If sBook <> Left(sTmp, iBookLen) Then
            sBook = Left(sTmp, iBookLen)
            sFilename = sMapDir & sBook & "\" & sBook & sPage & "." & g_sMapExt
            If sBook > "0" And sBook < sMaxBook Then
               If Dir(sFilename, FileAttribute.Normal) = "" Then
                  PrintLine(fhOut, sBook)
                  g_iMissingIndex = g_iMissingIndex + 1
                  LogMsg0(sFilename)
               End If
            End If
         End If
      Loop

      FileClose(fhIn)
      FileClose(fhOut)
      LogMsg("Number of missing index:      " & g_iMissingIndex)

      SFX_MissingIndex = True
      Exit Function

SFX_MissingIndex_Error:
      LogMsg("Error: " & Err.Description)
      SFX_MissingIndex = False
   End Function

   'SDX
   Public Function SDX_MissingMaps(ByVal sCntyCode As String) As Boolean
      Dim fhOut, fhIn, iRet, iCnt As Integer
      Dim iBookLen, iCmpLen, iPageLen, iBook, iSavePageLen, iSaveCmpLen As Integer
      Dim sTmp, sOneChar, sMapDir As String
      Dim sOutfile, sCmpfile, sFilename As String
      Dim asFlds() As String
      Dim sBook, sBookPage, sPage, sMaxBook As String
      Dim sPathInc As String

      On Error GoTo SDX_MissingMaps_Error

      'Get book/page len
      iBookLen = getBookLen(sCntyCode)
      iPageLen = getPageLen(sCntyCode)
      If iBookLen = 0 Or iPageLen = 0 Then
         MsgBox("Invalid Book/Page length.  Please check " & g_sCountyFile)
         Exit Function
      End If
      iCmpLen = iBookLen + iPageLen

      'Setup outfile
      sOutfile = g_sOutputDir.Replace("???", sCntyCode)
      If Dir(sOutfile, FileAttribute.Directory) = "" Then
         MkDir((sOutfile))
      End If
      sOutfile = sOutfile & "\" & sCntyCode & "_MissingMaps.log"
      fhOut = FreeFile()
      FileOpen(fhOut, sOutfile, OpenMode.Output)

      'Open CMP file
      sCmpfile = g_sDataDir & "\S" & sCntyCode & "assr\S" & sCntyCode & ".cmp"
      fhIn = FreeFile()
      FileOpen(fhIn, sCmpfile, OpenMode.Input)

      'Setup map folder
      sBookPage = " "
      sMapDir = g_sMapDir & "\" & sCntyCode & "\BK"
      g_iMissingMap = 0
      iCnt = 0
      sMaxBook = getCountyMaxBook()
      iSavePageLen = iPageLen
      iSaveCmpLen = iCmpLen

      'Loop through all record in CMP
      Do While Not EOF(fhIn)
         sTmp = LineInput(fhIn)
         iCnt = iCnt + 1
         sBook = Left(sTmp, iBookLen)
         iBook = CInt(sBook)
         If sBook = "760" Then
            iPageLen = iSavePageLen + 1
            iCmpLen = iSaveCmpLen + 1
         Else
            iPageLen = iSavePageLen
            iCmpLen = iSaveCmpLen
         End If

         If sBookPage <> Left(sTmp, iCmpLen) And sBook < sMaxBook Then
            sBookPage = Left(sTmp, iCmpLen)
            'If sBook = "760" Then
            '   sPage = Mid(sTmp, iBookLen + 1, iPageLen + 1)
            '   sBookPage = Left(sTmp, iCmpLen + 1)
            'Else
            '   sPage = Mid(sTmp, iBookLen + 1, iPageLen)
            'End If

            sPage = Mid(sTmp, iBookLen + 1, iPageLen)
            sFilename = sMapDir & sBook & "\" & sBook & sPage & "." & g_sMapExt
            If Dir(sFilename, FileAttribute.Normal) = "" Then
               PrintLine(fhOut, sBookPage)
               g_iMissingMap = g_iMissingMap + 1
               LogMsg0(sFilename)
            End If
         End If
      Loop

      LogMsg("Number of records processed: " & iCnt)
      LogMsg("Number of missing maps:      " & g_iMissingMap)
      FileClose(fhIn)
      FileClose(fhOut)
      SDX_MissingMaps = True
      Exit Function

SDX_MissingMaps_Error:
      LogMsg("Error: " & Err.Description)
      SDX_MissingMaps = False
   End Function

   Public Function SDX_MissingIndex(ByVal sCntyCode As String) As Boolean
      Dim fhOut, fhIn, iRet As Integer
      Dim iBookLen, iPageLen As Integer
      Dim sTmp, sMapDir As String
      Dim sOutfile, sCmpfile, sFilename As String
      Dim asFlds() As String
      Dim sBook, sPage, sMaxBook As String

      On Error GoTo SDX_MissingIndex_Error

      'Get book/page len
      iBookLen = getBookLen(sCntyCode)
      iPageLen = getPageLen(sCntyCode)
      If iBookLen = 0 Or iPageLen = 0 Then
         MsgBox("Invalid Book/Page length.  Please check " & g_sCountyFile)
         Exit Function
      End If

      'Setup outfile
      sOutfile = g_sOutputDir.Replace("???", sCntyCode)
      If Dir(sOutfile, FileAttribute.Directory) = "" Then
         MkDir((sOutfile))
      End If
      sOutfile = sOutfile & "\" & sCntyCode & "_MissingIndex.log"
      fhOut = FreeFile()
      FileOpen(fhOut, sOutfile, OpenMode.Output)

      'Open CMP file
      sCmpfile = g_sDataDir & "\S" & sCntyCode & "assr\S" & sCntyCode & ".cmp"
      fhIn = FreeFile()
      FileOpen(fhIn, sCmpfile, OpenMode.Input)

      'Setup map folder
      sMapDir = g_sMapDir & "\" & sCntyCode & "\BK"
      sPage = Left("0000", iPageLen)

      'Loop through all record in CMP
      sBook = " "
      sMaxBook = getCountyMaxBook()
      g_iMissingIndex = 0
      Do While Not EOF(fhIn)
         sTmp = LineInput(fhIn)
         If sBook <> Left(sTmp, iBookLen) Then
            sBook = Left(sTmp, iBookLen)
            If sBook = "760" Then
               sPage = Left("0000", iPageLen + 1)
            Else
               sPage = Left("0000", iPageLen)
            End If
            sFilename = sMapDir & sBook & "\" & sBook & sPage & "." & g_sMapExt
            If sBook > "0" And sBook < sMaxBook Then
               If Dir(sFilename, FileAttribute.Normal) = "" Then
                  PrintLine(fhOut, sBook)
                  g_iMissingIndex = g_iMissingIndex + 1
                  LogMsg0(sFilename)
               End If
            End If
         End If
      Loop

      FileClose(fhIn)
      FileClose(fhOut)
      LogMsg("Number of missing index:      " & g_iMissingIndex)

      SDX_MissingIndex = True
      Exit Function

SDX_MissingIndex_Error:
      LogMsg("Error: " & Err.Description)
      SDX_MissingIndex = False
   End Function

   'FRE
   Public Function FRE_MissingMaps(ByVal sCntyCode As String) As Boolean
      Dim fhOut, fhIn, iRet, iCnt As Integer
      Dim iBookLen, iCmpLen, iPageLen, iBook, iSavePageLen, iSaveCmpLen As Integer
      Dim sTmp, sOneChar, sMapDir As String
      Dim sOutfile, sCmpfile, sFilename As String
      Dim asFlds() As String
      Dim sBook, sBookPage, sPage, sMaxBook As String
      Dim sPathInc As String

      On Error GoTo FRE_MissingMaps_Error

      'Get book/page len
      iBookLen = getBookLen(sCntyCode)
      iPageLen = getPageLen(sCntyCode)
      If iBookLen = 0 Or iPageLen = 0 Then
         MsgBox("Invalid Book/Page length.  Please check " & g_sCountyFile)
         Exit Function
      End If
      iCmpLen = iBookLen + iPageLen

      'Setup outfile
      sOutfile = g_sOutputDir.Replace("???", sCntyCode)
      If Dir(sOutfile, FileAttribute.Directory) = "" Then
         MkDir((sOutfile))
      End If
      sOutfile = sOutfile & "\" & sCntyCode & "_MissingMaps.log"
      fhOut = FreeFile()
      FileOpen(fhOut, sOutfile, OpenMode.Output)

      'Open CMP file
      sCmpfile = g_sDataDir & "\S" & sCntyCode & "assr\S" & sCntyCode & ".cmp"
      fhIn = FreeFile()
      FileOpen(fhIn, sCmpfile, OpenMode.Input)

      'Setup map folder
      sBookPage = " "
      sMapDir = g_sMapDir & "\" & sCntyCode & "\BK"
      g_iMissingMap = 0
      iCnt = 0
      sMaxBook = getCountyMaxBook()
      iSavePageLen = iPageLen
      iSaveCmpLen = iCmpLen

      'Loop through all record in CMP
      Do While Not EOF(fhIn)
         sTmp = LineInput(fhIn)
         iCnt = iCnt + 1
         sBook = Left(sTmp, iBookLen)
         iBook = CInt(sBook)
         If iBook >= 700 And iBook <= 800 Then
            iPageLen = iSavePageLen + 1
            iCmpLen = iSaveCmpLen + 1
         Else
            iPageLen = iSavePageLen
            iCmpLen = iSaveCmpLen
         End If
         If sBookPage <> Left(sTmp, iCmpLen) And sBook < sMaxBook Then
            sBookPage = Left(sTmp, iCmpLen)
            sPage = Mid(sTmp, iBookLen + 1, iPageLen)
            sFilename = sMapDir & sBook & "\" & sBook & sPage & "." & g_sMapExt
            If Dir(sFilename, FileAttribute.Normal) = "" Then
               PrintLine(fhOut, sBook & sPage)
               g_iMissingMap += 1
               LogMsg0(sFilename)
            End If
         End If
      Loop

      LogMsg("Number of records processed: " & iCnt)
      LogMsg("Number of missing maps:      " & g_iMissingMap)
      FileClose(fhIn)
      FileClose(fhOut)
      FRE_MissingMaps = True
      Exit Function

FRE_MissingMaps_Error:
      LogMsg("Error: " & Err.Description)
      FRE_MissingMaps = False
   End Function

   Public Function FRE_MissingIndex(ByVal sCntyCode As String) As Boolean
      Dim fhOut, fhIn, iRet As Integer
      Dim iBookLen, iPageLen, iBook As Integer
      Dim sTmp, sMapDir As String
      Dim sOutfile, sCmpfile, sFilename As String
      Dim asFlds() As String
      Dim sBook, sPage, sMaxBook As String

      On Error GoTo FRE_MissingIndex_Error

      'Get book/page len
      iBookLen = getBookLen(sCntyCode)
      iPageLen = getPageLen(sCntyCode)
      If iBookLen = 0 Or iPageLen = 0 Then
         MsgBox("Invalid Book/Page length.  Please check " & g_sCountyFile)
         Exit Function
      End If

      'Setup outfile
      sOutfile = g_sOutputDir.Replace("???", sCntyCode)
      If Dir(sOutfile, FileAttribute.Directory) = "" Then
         MkDir((sOutfile))
      End If
      sOutfile = sOutfile & "\" & sCntyCode & "_MissingIndex.log"
      fhOut = FreeFile()
      FileOpen(fhOut, sOutfile, OpenMode.Output)

      'Open CMP file
      sCmpfile = g_sDataDir & "\S" & sCntyCode & "assr\S" & sCntyCode & ".cmp"
      fhIn = FreeFile()
      FileOpen(fhIn, sCmpfile, OpenMode.Input)

      'Setup map folder
      sMapDir = g_sMapDir & "\" & sCntyCode & "\BK"
      sPage = Left("0000", iPageLen)

      'Loop through all record in CMP
      sBook = " "
      sMaxBook = getCountyMaxBook()
      g_iMissingIndex = 0
      Do While Not EOF(fhIn)
         sTmp = LineInput(fhIn)
         If sBook <> Left(sTmp, iBookLen) Then
            sBook = Left(sTmp, iBookLen)
            iBook = CInt(sBook)
            If iBook >= 700 And iBook <= 800 Then
               sPage = Left("0000", iPageLen + 1)
            Else
               sPage = Left("0000", iPageLen)
            End If
            sFilename = sMapDir & sBook & "\" & sBook & sPage & "." & g_sMapExt
            If sBook > "0" And sBook < sMaxBook Then
               If Dir(sFilename, FileAttribute.Normal) = "" Then
                  PrintLine(fhOut, sBook)
                  g_iMissingIndex = g_iMissingIndex + 1
                  LogMsg0(sFilename)
               End If
            End If
         End If
      Loop

      FileClose(fhIn)
      FileClose(fhOut)
      LogMsg("Number of missing index:      " & g_iMissingIndex)

      FRE_MissingIndex = True
      Exit Function

FRE_MissingIndex_Error:
      LogMsg("Error: " & Err.Description)
      FRE_MissingIndex = False
   End Function

End Module