Option Strict Off
Option Explicit On
Friend Class frmMapCheck
	Inherits System.Windows.Forms.Form
#Region "Windows Form Designer generated code "
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'For the start-up form, the first instance created is the default instance.
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdFindAll As System.Windows.Forms.Button
	Public WithEvents cbPathInc As System.Windows.Forms.CheckBox
	Public WithEvents cbMapExt As System.Windows.Forms.ComboBox
   Public WithEvents cmdAllMissing As System.Windows.Forms.Button
   Public WithEvents cmdFindMaps As System.Windows.Forms.Button
   Public WithEvents txtOutfile As System.Windows.Forms.TextBox
   Public WithEvents cmdFindIndex As System.Windows.Forms.Button
   Public WithEvents txtDataFolder As System.Windows.Forms.TextBox
   Public WithEvents txtMapFolder As System.Windows.Forms.TextBox
   Public WithEvents Label4 As System.Windows.Forms.Label
   Public WithEvents Label2 As System.Windows.Forms.Label
   Friend WithEvents cbCounty As System.Windows.Forms.ComboBox
   Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
   Public WithEvents Label1 As System.Windows.Forms.Label
   'NOTE: The following procedure is required by the Windows Form Designer
   'It can be modified using the Windows Form Designer.
   'Do not modify it using the code editor.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
      Me.components = New System.ComponentModel.Container()
      Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMapCheck))
      Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
      Me.cmdFindAll = New System.Windows.Forms.Button()
      Me.cbPathInc = New System.Windows.Forms.CheckBox()
      Me.cbMapExt = New System.Windows.Forms.ComboBox()
      Me.cmdAllMissing = New System.Windows.Forms.Button()
      Me.cmdFindMaps = New System.Windows.Forms.Button()
      Me.cmdFindIndex = New System.Windows.Forms.Button()
      Me.txtOutfile = New System.Windows.Forms.TextBox()
      Me.txtDataFolder = New System.Windows.Forms.TextBox()
      Me.txtMapFolder = New System.Windows.Forms.TextBox()
      Me.Label4 = New System.Windows.Forms.Label()
      Me.Label2 = New System.Windows.Forms.Label()
      Me.Label1 = New System.Windows.Forms.Label()
      Me.cbCounty = New System.Windows.Forms.ComboBox()
      Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
      Me.SuspendLayout()
      '
      'cmdFindAll
      '
      Me.cmdFindAll.BackColor = System.Drawing.SystemColors.Control
      Me.cmdFindAll.Cursor = System.Windows.Forms.Cursors.Default
      Me.cmdFindAll.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.cmdFindAll.ForeColor = System.Drawing.SystemColors.ControlText
      Me.cmdFindAll.Location = New System.Drawing.Point(384, 192)
      Me.cmdFindAll.Name = "cmdFindAll"
      Me.cmdFindAll.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.cmdFindAll.Size = New System.Drawing.Size(89, 25)
      Me.cmdFindAll.TabIndex = 12
      Me.cmdFindAll.Text = "&List All"
      Me.ToolTip1.SetToolTip(Me.cmdFindAll, "List all possible maps")
      Me.cmdFindAll.UseVisualStyleBackColor = False
      '
      'cbPathInc
      '
      Me.cbPathInc.BackColor = System.Drawing.SystemColors.Control
      Me.cbPathInc.Cursor = System.Windows.Forms.Cursors.Default
      Me.cbPathInc.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.cbPathInc.ForeColor = System.Drawing.SystemColors.ControlText
      Me.cbPathInc.Location = New System.Drawing.Point(400, 88)
      Me.cbPathInc.Name = "cbPathInc"
      Me.cbPathInc.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.cbPathInc.Size = New System.Drawing.Size(89, 17)
      Me.cbPathInc.TabIndex = 11
      Me.cbPathInc.Text = "Include Path"
      Me.ToolTip1.SetToolTip(Me.cbPathInc, "Include path start with BK")
      Me.cbPathInc.UseVisualStyleBackColor = False
      '
      'cbMapExt
      '
      Me.cbMapExt.BackColor = System.Drawing.SystemColors.Window
      Me.cbMapExt.Cursor = System.Windows.Forms.Cursors.Default
      Me.cbMapExt.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.cbMapExt.ForeColor = System.Drawing.SystemColors.WindowText
      Me.cbMapExt.Items.AddRange(New Object() {"PQM", "TIF", "PDF"})
      Me.cbMapExt.Location = New System.Drawing.Point(400, 136)
      Me.cbMapExt.Name = "cbMapExt"
      Me.cbMapExt.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.cbMapExt.Size = New System.Drawing.Size(49, 22)
      Me.cbMapExt.TabIndex = 10
      Me.cbMapExt.Text = "PQM"
      Me.ToolTip1.SetToolTip(Me.cbMapExt, "Map extension to look for")
      '
      'cmdAllMissing
      '
      Me.cmdAllMissing.BackColor = System.Drawing.SystemColors.Control
      Me.cmdAllMissing.Cursor = System.Windows.Forms.Cursors.Default
      Me.cmdAllMissing.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.cmdAllMissing.ForeColor = System.Drawing.SystemColors.ControlText
      Me.cmdAllMissing.Location = New System.Drawing.Point(264, 192)
      Me.cmdAllMissing.Name = "cmdAllMissing"
      Me.cmdAllMissing.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.cmdAllMissing.Size = New System.Drawing.Size(89, 25)
      Me.cmdAllMissing.TabIndex = 8
      Me.cmdAllMissing.Text = "Missing &All"
      Me.ToolTip1.SetToolTip(Me.cmdAllMissing, "Run Index and Maps")
      Me.cmdAllMissing.UseVisualStyleBackColor = False
      '
      'cmdFindMaps
      '
      Me.cmdFindMaps.BackColor = System.Drawing.SystemColors.Control
      Me.cmdFindMaps.Cursor = System.Windows.Forms.Cursors.Default
      Me.cmdFindMaps.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.cmdFindMaps.ForeColor = System.Drawing.SystemColors.ControlText
      Me.cmdFindMaps.Location = New System.Drawing.Point(144, 192)
      Me.cmdFindMaps.Name = "cmdFindMaps"
      Me.cmdFindMaps.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.cmdFindMaps.Size = New System.Drawing.Size(89, 25)
      Me.cmdFindMaps.TabIndex = 7
      Me.cmdFindMaps.Text = "Missing &Maps"
      Me.ToolTip1.SetToolTip(Me.cmdFindMaps, "Find missing maps")
      Me.cmdFindMaps.UseVisualStyleBackColor = False
      '
      'cmdFindIndex
      '
      Me.cmdFindIndex.BackColor = System.Drawing.SystemColors.Control
      Me.cmdFindIndex.Cursor = System.Windows.Forms.Cursors.Default
      Me.cmdFindIndex.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.cmdFindIndex.ForeColor = System.Drawing.SystemColors.ControlText
      Me.cmdFindIndex.Location = New System.Drawing.Point(24, 192)
      Me.cmdFindIndex.Name = "cmdFindIndex"
      Me.cmdFindIndex.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.cmdFindIndex.Size = New System.Drawing.Size(89, 25)
      Me.cmdFindIndex.TabIndex = 4
      Me.cmdFindIndex.Text = "Missing &Index"
      Me.ToolTip1.SetToolTip(Me.cmdFindIndex, "Find missing index maps")
      Me.cmdFindIndex.UseVisualStyleBackColor = False
      '
      'txtOutfile
      '
      Me.txtOutfile.AcceptsReturn = True
      Me.txtOutfile.BackColor = System.Drawing.SystemColors.Window
      Me.txtOutfile.Cursor = System.Windows.Forms.Cursors.IBeam
      Me.txtOutfile.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.txtOutfile.ForeColor = System.Drawing.SystemColors.WindowText
      Me.txtOutfile.Location = New System.Drawing.Point(104, 136)
      Me.txtOutfile.MaxLength = 0
      Me.txtOutfile.Name = "txtOutfile"
      Me.txtOutfile.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.txtOutfile.Size = New System.Drawing.Size(257, 20)
      Me.txtOutfile.TabIndex = 5
      Me.txtOutfile.Text = "H:\CO_OUT"
      '
      'txtDataFolder
      '
      Me.txtDataFolder.AcceptsReturn = True
      Me.txtDataFolder.BackColor = System.Drawing.SystemColors.Window
      Me.txtDataFolder.Cursor = System.Windows.Forms.Cursors.IBeam
      Me.txtDataFolder.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.txtDataFolder.ForeColor = System.Drawing.SystemColors.WindowText
      Me.txtDataFolder.Location = New System.Drawing.Point(104, 88)
      Me.txtDataFolder.MaxLength = 0
      Me.txtDataFolder.Name = "txtDataFolder"
      Me.txtDataFolder.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.txtDataFolder.Size = New System.Drawing.Size(257, 20)
      Me.txtDataFolder.TabIndex = 3
      Me.txtDataFolder.Text = "H:\PQAssr"
      '
      'txtMapFolder
      '
      Me.txtMapFolder.AcceptsReturn = True
      Me.txtMapFolder.BackColor = System.Drawing.SystemColors.Window
      Me.txtMapFolder.Cursor = System.Windows.Forms.Cursors.IBeam
      Me.txtMapFolder.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.txtMapFolder.ForeColor = System.Drawing.SystemColors.WindowText
      Me.txtMapFolder.Location = New System.Drawing.Point(104, 40)
      Me.txtMapFolder.MaxLength = 0
      Me.txtMapFolder.Name = "txtMapFolder"
      Me.txtMapFolder.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.txtMapFolder.Size = New System.Drawing.Size(257, 20)
      Me.txtMapFolder.TabIndex = 1
      Me.txtMapFolder.Text = "H:\PQMAPS"
      '
      'Label4
      '
      Me.Label4.BackColor = System.Drawing.SystemColors.Control
      Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
      Me.Label4.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
      Me.Label4.Location = New System.Drawing.Point(11, 136)
      Me.Label4.Name = "Label4"
      Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.Label4.Size = New System.Drawing.Size(87, 17)
      Me.Label4.TabIndex = 6
      Me.Label4.Text = "Output Folder"
      Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'Label2
      '
      Me.Label2.BackColor = System.Drawing.SystemColors.Control
      Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
      Me.Label2.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
      Me.Label2.Location = New System.Drawing.Point(8, 88)
      Me.Label2.Name = "Label2"
      Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.Label2.Size = New System.Drawing.Size(90, 17)
      Me.Label2.TabIndex = 2
      Me.Label2.Text = "PQData Folder"
      Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'Label1
      '
      Me.Label1.BackColor = System.Drawing.SystemColors.Control
      Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
      Me.Label1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
      Me.Label1.Location = New System.Drawing.Point(24, 40)
      Me.Label1.Name = "Label1"
      Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.Label1.Size = New System.Drawing.Size(74, 17)
      Me.Label1.TabIndex = 0
      Me.Label1.Text = "Map Folder"
      Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'cbCounty
      '
      Me.cbCounty.FormattingEnabled = True
      Me.cbCounty.Items.AddRange(New Object() {"ALA", "ALP", "AMA", "BUT", "CAL", "COL", "CCX", "DNX", "EDX", "FRE", "GLE", "HUM", "IMP", "INY", "KER", "KIN", "LAK", "LAS", "LAX", "MAD", "MRN", "MPA", "MEN", "MER", "MOD", "MNO", "MON", "NAP", "NEV", "ORG", "PLA", "PLU", "RIV", "SAC", "SBT", "SBD", "SDX", "SFX", "SJX", "SLO", "SMX", "SBX", "SCL", "SCR", "SHA", "SIE", "SIS", "SOL", "SON", "STA", "SUT", "TEH", "TRI", "TUL", "TUO", "VEN", "YOL", "YUB"})
      Me.cbCounty.Location = New System.Drawing.Point(398, 40)
      Me.cbCounty.Name = "cbCounty"
      Me.cbCounty.Size = New System.Drawing.Size(51, 22)
      Me.cbCounty.TabIndex = 13
      '
      'StatusStrip1
      '
      Me.StatusStrip1.Location = New System.Drawing.Point(0, 239)
      Me.StatusStrip1.Name = "StatusStrip1"
      Me.StatusStrip1.Size = New System.Drawing.Size(503, 22)
      Me.StatusStrip1.TabIndex = 14
      '
      'frmMapCheck
      '
      Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
      Me.BackColor = System.Drawing.SystemColors.Control
      Me.ClientSize = New System.Drawing.Size(503, 261)
      Me.Controls.Add(Me.StatusStrip1)
      Me.Controls.Add(Me.cbCounty)
      Me.Controls.Add(Me.cmdFindAll)
      Me.Controls.Add(Me.cbPathInc)
      Me.Controls.Add(Me.cbMapExt)
      Me.Controls.Add(Me.cmdAllMissing)
      Me.Controls.Add(Me.cmdFindMaps)
      Me.Controls.Add(Me.txtOutfile)
      Me.Controls.Add(Me.cmdFindIndex)
      Me.Controls.Add(Me.txtDataFolder)
      Me.Controls.Add(Me.txtMapFolder)
      Me.Controls.Add(Me.Label4)
      Me.Controls.Add(Me.Label2)
      Me.Controls.Add(Me.Label1)
      Me.Cursor = System.Windows.Forms.Cursors.Default
      Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
      Me.Location = New System.Drawing.Point(4, 23)
      Me.Name = "frmMapCheck"
      Me.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.Text = "Map Maintenance 1.0"
      Me.ResumeLayout(False)
      Me.PerformLayout()

   End Sub
#End Region
#Region "Upgrade Support "
   Private Shared m_vb6FormDefInstance As frmMapCheck
   Private Shared m_InitializingDefInstance As Boolean
   Public Shared Property DefInstance() As frmMapCheck
      Get
         If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
            m_InitializingDefInstance = True
            m_vb6FormDefInstance = New frmMapCheck()
            m_InitializingDefInstance = False
         End If
         DefInstance = m_vb6FormDefInstance
      End Get
      Set(value As frmMapCheck)
         m_vb6FormDefInstance = Value
      End Set
   End Property
#End Region

   Private Sub initGlobal()
      g_sMapDir = txtMapFolder.Text
      g_sDataDir = txtDataFolder.Text
      g_sOutputDir = txtOutfile.Text
      g_sMapExt = cbMapExt.Text
      'g_sDataDir.Replace("???", txtCounty.Text)
   End Sub
   Private Sub cmdAllMissing_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAllMissing.Click
      Call cmdFindIndex_Click(cmdFindIndex, New System.EventArgs())
      Call cmdFindMaps_Click(cmdFindMaps, New System.EventArgs())
   End Sub

   Private Sub cmdFindAll_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFindAll.Click
      Dim bRet As Boolean
      Dim iCnt As Integer
      Dim sCnty As String

      On Error GoTo cmdFindAll_Error

      Call initGlobal()

      Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
      cmdFindIndex.Enabled = False
      cmdFindMaps.Enabled = False
      cmdAllMissing.Enabled = False
      bRet = False

      'Processing
      iCnt = 1
      sCnty = cbCounty.Text
      bRet = ListAllMaps(sCnty, cbPathInc.CheckState)

      If bRet = False Then
         MsgBox("Error finding missing maps for " & sCnty)
      End If
      GoTo cmdFindAll_Exit

cmdFindAll_Error:
      MsgBox("Error: " & Err.Description)

cmdFindAll_Exit:
      cmdFindIndex.Enabled = True
      cmdFindMaps.Enabled = True
      cmdAllMissing.Enabled = True
      Me.Cursor = System.Windows.Forms.Cursors.Default
   End Sub

   'Search for index maps.  Log missing ones
   Private Sub cmdFindIndex_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFindIndex.Click
      Dim bRet As Boolean
      Dim iCnt As Integer
      Dim sCnty As String

      Call initGlobal()

      Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
      cmdFindIndex.Enabled = False
      cmdFindMaps.Enabled = False
      cmdAllMissing.Enabled = False
      bRet = False

      'Processing
      iCnt = 1
      sCnty = cbCounty.Text
      If sCnty = "ALA" Then
         bRet = Ex1MissingIndex(sCnty)
      Else
         bRet = FindMissingIndex(sCnty)
      End If

      If bRet = False Then
         MsgBox("Error finding index map for " & cbCounty.Text)
         StatusStrip1.Text = "Missing Index: " & g_iMissingIndex
      End If
      GoTo cmdFindIndex_Exit

cmdFindIndex_Error:
      MsgBox("Error: " & Err.Description)

cmdFindIndex_Exit:
      cmdFindIndex.Enabled = True
      cmdFindMaps.Enabled = True
      cmdAllMissing.Enabled = True
      Me.Cursor = System.Windows.Forms.Cursors.Default
   End Sub

   Private Sub cmdFindMaps_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFindMaps.Click
      Dim bRet As Boolean
      Dim iCnt As Integer
      Dim sCnty As String

      Call initGlobal()

      Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
      cmdFindIndex.Enabled = False
      cmdFindMaps.Enabled = False
      cmdAllMissing.Enabled = False
      bRet = False

      'Processing
      iCnt = 1
      sCnty = cbCounty.Text
      If sCnty = "ALL" Then
         Do While True
            sCnty = getCountyCode(iCnt)
            If sCnty > "A" Then
               bRet = FindMissingMaps(sCnty, cbPathInc.CheckState)
               If sCnty = "ALA" Then
                  bRet = Ex1MissingMaps(sCnty)
               ElseIf sCnty = "LAK" Then
                  bRet = LAK_MissingMaps(sCnty)
               Else
                  bRet = FindMissingMaps(sCnty, cbPathInc.CheckState)
               End If
               iCnt = iCnt + 1
            Else
               Exit Do
            End If
         Loop
      Else
         If sCnty = "ALA" Then
            bRet = Ex1MissingMaps(sCnty)
         ElseIf sCnty = "LAK" Then
            bRet = LAK_MissingMaps(sCnty)
         Else
            bRet = FindMissingMaps(sCnty, cbPathInc.CheckState)
         End If
      End If

      If bRet = False Then
         MsgBox("Error finding missing maps for " & sCnty)
      End If
      GoTo cmdFindMaps_Exit

cmdFindMaps_Error:
      MsgBox("Error: " & Err.Description)

cmdFindMaps_Exit:
      cmdFindIndex.Enabled = True
      cmdFindMaps.Enabled = True
      cmdAllMissing.Enabled = True
      Me.Cursor = System.Windows.Forms.Cursors.Default
   End Sub

   Private Sub frmMapCheck_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
      cbCounty.SelectedIndex = 0
      g_sCounty = cbCounty.SelectedItem

      'Set default data folder
      txtDataFolder.Text = g_sDataDir.Replace("???", g_sCounty)
      txtMapFolder.Text = g_sMapDir
      txtOutfile.Text = g_sOutputDir
      cbMapExt.Text = g_sMapExt
      Me.Text = "Map Maintenance " & System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly.Location).FileMajorPart & "." & System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly.Location).FileMinorPart
   End Sub

   Private Sub cbCounty_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbCounty.SelectedIndexChanged
      Dim sTmp As String

      g_sDataDir = My.Settings.DataDir
      sTmp = g_sDataDir.Replace("???", cbCounty.SelectedItem)
      txtDataFolder.Text = sTmp
   End Sub

End Class