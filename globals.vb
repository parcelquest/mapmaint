Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Module modMain
   Public g_sCounty As String
	Public g_sMapDir As String
	Public g_sDataDir As String
	Public g_sOutputDir As String
	Public g_sCountyFile As String
   Public g_sMapExt As String
   Public g_iMissingIndex As Integer
   Public g_iMissingMap As Integer

   Public Sub Main()
      Dim bRet As Boolean
      Dim sCmd, sCounty(64) As String
      Dim iCnt, iCntyID As Integer

      'We don't want multiple copy to run at the same time
      If (UBound(Diagnostics.Process.GetProcessesByName(Diagnostics.Process.GetCurrentProcess.ProcessName)) > 0) Then Exit Sub

      'Get INI settings
      InitSettings() 'Initialize settings

      'Load county info
      If Not LoadCountyInfo(g_sCountyFile) Then
         LogMsg("Error loading county info " & g_sCountyFile & ": " & Err.Description)
         Exit Sub
      End If

      LogMsg("MapMaintenance " & My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & "." & My.Application.Info.Version.Revision)

      'Check command line
      sCmd = VB.Command()
      If sCmd = "" Then
         frmMapCheck.DefInstance.ShowDialog()
      Else
         ' Parse command line
         If sCmd = "ALL" Then
            iCnt = 1
            Do While True
               g_sCounty = getCountyCode(iCnt)
               If g_sCounty > "A" Then
                  g_sDataDir = My.Settings.DataDir.Replace("???", g_sCounty)
                  iCntyID = setCurrentCounty(g_sCounty)
                  If g_sCounty = "ALA" Then
                     bRet = Ex1MissingIndex(g_sCounty)
                     bRet = Ex1MissingMaps(g_sCounty)
                  ElseIf g_sCounty = "LAK" Then
                     bRet = FindMissingIndex(g_sCounty)
                     bRet = LAK_MissingMaps(g_sCounty)
                  ElseIf g_sCounty = "SBX" Then
                     bRet = FindMissingIndex(g_sCounty)
                     bRet = SBX_MissingMaps(g_sCounty)
                  Else
                     bRet = FindMissingIndex(g_sCounty)
                     bRet = FindMissingMaps(g_sCounty, False)
                  End If
                  iCnt = iCnt + 1
               Else
                  Exit Do
               End If
            Loop
            LogMsg("There are " & iCnt & " counties processed")
         Else
            sCounty = sCmd.Split(" ")
            For iCnt = 0 To sCounty.Length - 1
               g_sCounty = sCounty(iCnt)
               If g_sCounty > "A" Then
                  g_sDataDir = My.Settings.DataDir.Replace("???", g_sCounty)
                  iCntyID = setCurrentCounty(g_sCounty)
                  If iCntyID = 0 Then
                     LogMsg("***** Invalid county code: " & g_sCounty)
                     Exit For
                  End If
                  LogMsg("Find Missing index for : " & g_sCounty)
                  If g_sCounty = "ALA" Then
                     bRet = Ex1MissingIndex(g_sCounty)
                     LogMsg("Find Missing maps for : " & g_sCounty)
                     bRet = Ex1MissingMaps(g_sCounty)
                  ElseIf g_sCounty = "FRE" Then
                     bRet = FRE_MissingIndex(g_sCounty)
                     LogMsg("Find Missing maps for : " & g_sCounty)
                     bRet = FRE_MissingMaps(g_sCounty)
                  ElseIf g_sCounty = "LAK" Then
                     bRet = FindMissingIndex(g_sCounty)
                     LogMsg("Find Missing maps for : " & g_sCounty)
                     bRet = LAK_MissingMaps(g_sCounty)
                  ElseIf g_sCounty = "SBX" Then
                     bRet = FindMissingIndex(g_sCounty)
                     LogMsg("Find Missing maps for : " & g_sCounty)
                     bRet = SBX_MissingMaps(g_sCounty)
                  ElseIf g_sCounty = "SDX" Then
                     bRet = SDX_MissingIndex(g_sCounty)
                     LogMsg("Find Missing maps for : " & g_sCounty)
                     bRet = SDX_MissingMaps(g_sCounty)
                  ElseIf g_sCounty = "SFX" Then
                     bRet = SFX_MissingIndex(g_sCounty)
                     LogMsg("Find Missing maps for : " & g_sCounty)
                     bRet = SFX_MissingMaps(g_sCounty)
                  Else
                     bRet = FindMissingIndex(g_sCounty)
                     LogMsg("Find Missing maps for : " & g_sCounty)
                     bRet = FindMissingMaps(g_sCounty, False)
                  End If
               Else
                  Exit For
               End If
            Next
            LogMsg("There are " & iCnt & " counties processed")
         End If

      End If

      LogMsg("Processing finishes at " & Format(Now, "MM/dd/yyyy hh:mm"))
      End
   End Sub
	
	Private Function InitSettings() As Boolean
      InitSettings = True
      g_sMapDir = My.Settings.MapDir
      g_sMapExt = My.Settings.MapExt
      g_sDataDir = My.Settings.DataDir
      g_sCountyFile = My.Settings.CountyFile
      g_sOutputDir = My.Settings.OutDir
      g_logFile = My.Settings.Logfile
   End Function
End Module